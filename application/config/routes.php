<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['contoh'] = 'C_contoh_controller';
$route['contoh/(:any)'] = 'C_contoh_controller/$1';

// icd 9
$route['laporan_icd9'] = 'laporan/laporan_icd9/C_laporan_icd9';
$route['laporan_icd9/(:any)'] = 'laporan/laporan_icd9/C_laporan_icd9/$1';

// icd 10
$route['laporan_icd10'] = 'laporan/laporan_icd10/C_laporan_icd10';
$route['laporan_icd10/(:any)'] = 'laporan/laporan_icd10/C_laporan_icd10/$1';

// jasmed
$route['jasmed'] = 'jasmed/C_jasmed';
$route['jasmed/(:any)'] = 'jasmed/C_jasmed/$1';