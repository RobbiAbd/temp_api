<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_contoh_controller extends MY_controller
{
  // Parameter yang akan dipakain di endpoint ini
	private $param = [
		'periode_start',
		'periode_end',
		'sortby_column',
		'sortby_type',
		'offset',
		'limit',
		'search'
	];

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_contoh_model', 'model');
	}

  // Untuk validasi format tanggal di form validation
	public function valid_date($date)
	{
		$d = DateTime::createFromFormat('Y-m-d', $date);
		return $d && $d->format('Y-m-d') === $date;
	}

	public function index_get()
	{
		$get = $this->get();
		$this->load->library('form_validation');

    // cek paramter yang diperbolehkan digunakan di endpoint ini
		foreach ($get as $key => $val) {
			if (!in_array($key, $this->param)) {
				$res['status'] = 500;
				$res['message'] = "Error ! Parameter '" .  $key . "' tidak diperbolehkan !!!";
				$this->response($res, $res['status']);
			}
		}

    // form_validation untuk setiap parameter
		$this->form_validation->set_data($get);
		$this->form_validation->set_rules('periode_start', 'Periode Start', 'callback_valid_date', [
			'valid_date' => 'Format tanggal {field} tidak valid !'
		]);
		$this->form_validation->set_rules('periode_end', 'Periode End', 'callback_valid_date', [
			'valid_date' => 'Format tanggal {field} tidak valid !'
		]);
		$this->form_validation->set_rules('sortby_column', 'Sort By Coulumn', 'numeric', [
			'numeric' => 'Parameter {field}  hanya boleh numeric !'
		]);
		$this->form_validation->set_rules('sortby_type', 'Sort Type', 'in_list[ASC,DESC]', [
			'in_list' => 'Parameter {field}  hanya boleh diisi : ASC / DESC !!!'
		]);
		$this->form_validation->set_rules('offset', 'Offset', 'numeric', [
			'numeric' => 'Parameter {field}  hanya boleh numeric !'
		]);
		$this->form_validation->set_rules('limit', 'Limit', 'greater_than_equal_to[1]|less_than_equal_to[200]|numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
			'greater_than_equal_to' => 'Parameter {field}  minimal 1 !',
			'less_than_equal_to' => 'Parameter {field}  maksimal 200 !'
		]);

    // jika semua sdh oke, maka params akan diproses
		if ($this->form_validation->run() == true) {
			// yang wajib
			if (empty($get['periode_start'])) {
				$res['status']  = 400;
				$res['message'] = "Masukan periode start";
				$res['data']    = [];

				$this->response($res, $res['status']);
			}

			if (empty($get['periode_end'])) {
				$res['status']  = 400;
				$res['message'] = "Masukan periode end";
				$res['data']    = [];

				$this->response($res, $res['status']);
			}

			if (empty($get['search'])) {
				$get['search'] = '-';
			}

			if (empty($get['sortby_type'])) {
				$res['status']  = 400;
				$res['message'] = "Cara mengurutan (ASC / DESC)";
				$res['data']    = [];

				$this->response($res, $res['status']);
			}

			if ($get['offset'] == '' ||  $get['offset'] == null) {
				$res['status']  = 400;
				$res['message'] = "Masukan offset";
				$res['data']    = [];

				$this->response($res, $res['status']);
			}

			if ($get['limit'] == '' ||  $get['limit'] == null) {
				$res['status']  = 400;
				$res['message'] = "Masukan limit";
				$res['data']    = [];

				$this->response($res, $res['status']);
			}

			$data = $this->model->get($get);

			if (empty($data)) {
				$res['status']  = 404;
				$res['message'] = "Data tidak tersedia...";
				$res['data']    = $data;
			} else {
				$res['status']  = 200;
				$res['message'] = "Berhasil mendapatkan data.";
				$res['data']    = $data;
			}
			$this->response($res, $res['status']);
		}

    // jika type paramter salah, akan ditendang

		$res['status']  = 500;
		$errors = $this->form_validation->error_array();
		$res['message'] = $errors;
		$this->response($res, $res['status']);
	}
}
