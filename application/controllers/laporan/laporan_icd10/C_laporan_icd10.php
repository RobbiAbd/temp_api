<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class C_laporan_icd10 extends RestController
{
	private $param = [
		'jenis_periode',
        'periode_start',
        'periode_end',
        'sortby_column',
		'sortby_type',
		'offset',
		'limit',
		'search'
	];

	function __construct()
	{
		parent::__construct();
		$this->load->model('Model10/M_laporan_icd10', 'M_laporan_icd10');
	}

	// Untuk validasi format tanggal di form validation
	public function valid_date($date)
	{
		$d = DateTime::createFromFormat('Y-m-d', $date);
		return $d && $d->format('Y-m-d') === $date;
	}

	public function index_get()
	{
		$get = $this->get();
		$this->load->library('form_validation');

		foreach ($get as $key => $val) {
			if (!in_array($key, $this->param)) {
				$res['status'] = 500;
				$res['message'] = "Error ! Parameter '" .  $key . "' tidak diperbolehkan !!!";
				$this->response($res, $res['status']);
			}
		}

		$this->form_validation->set_data($get);

		$this->form_validation->set_rules('jenis_periode', 'Jenis Periode', 'numeric', [
			'numeric' => 'Parameter {field}  hanya boleh numeric !'
		]);
		$this->form_validation->set_rules('periode_start', 'Periode Start', 'callback_valid_date', [
			'valid_date' => 'Format tanggal {field} tidak valid !'
		]);
		$this->form_validation->set_rules('periode_end', 'Periode End', 'callback_valid_date', [
			'valid_date' => 'Format tanggal {field} tidak valid !'
		]);
		$this->form_validation->set_rules('sortby_column', 'Sort By Coulumn', 'numeric', [
			'numeric' => 'Parameter {field}  hanya boleh numeric !'
		]);
		$this->form_validation->set_rules('sortby_type', 'Sort Type', 'in_list[ASC,DESC]', [
			'in_list' => 'Parameter {field}  hanya boleh diisi : ASC / DESC !!!'
		]);
		$this->form_validation->set_rules('offset', 'Offset', 'numeric', [
			'numeric' => 'Parameter {field}  hanya boleh numeric !'
		]);
		$this->form_validation->set_rules('limit', 'Limit', 'greater_than_equal_to[1]|less_than_equal_to[200]|numeric', [
      'numeric' => 'Parameter {field}  hanya boleh diisi angka !',
			'greater_than_equal_to' => 'Parameter {field}  minimal 1 !',
			'less_than_equal_to' => 'Parameter {field}  maksimal 200 !'
		]);

    // jika semua sdh oke, maka params akan diproses
		if ($this->form_validation->run() == true) {
			// yang wajib
			if (empty($get['periode_start'])) {
				$res['status']  = 400;
				$res['message'] = "Masukan periode start";
				$res['data']    = [];

				$this->response($res, $res['status']);
			}

			if (empty($get['periode_end'])) {
				$res['status']  = 400;
				$res['message'] = "Masukan periode end";
				$res['data']    = [];

				$this->response($res, $res['status']);
			}

			if (empty($get['search'])) {
				$get['search'] = '-';
			}

			if (empty($get['sortby_type'])) {
				$res['status']  = 400;
				$res['message'] = "Cara mengurutan (ASC / DESC)";
				$res['data']    = [];

				$this->response($res, $res['status']);
			}

			if ($get['offset'] == '' ||  $get['offset'] == null) {
				$res['status']  = 400;
				$res['message'] = "Masukan offset";
				$res['data']    = [];

				$this->response($res, $res['status']);
			}

			if ($get['limit'] == '' ||  $get['limit'] == null) {
				$res['status']  = 400;
				$res['message'] = "Masukan limit";
				$res['data']    = [];

				$this->response($res, $res['status']);
			}

			$data = $this->M_laporan_icd10->get($get);

			if (empty($data)) {
				$res['status']  = 404;
				$res['message'] = "Data tidak tersedia...";
				$res['data']    = $data;
			} else {
				$res['status']  = 200;
				$res['message'] = "Berhasil mendapatkan data.";
				$res['total_data'] = $data['record_total'];

				$dataResponse = [];
				foreach ($data['result'] as $val) {
					$primary = $val->xprimary == 1 ? 'primary' : 'secondary';
					$kelamin = $val->gender;
					if (!isset($dataResponse[$val->code_icd10])) {
						$dataResponse[$val->code_icd10] = [
		                    'deskripsi' => $val->nama_icd10,
		                    'primary' => ['total' => [
			                        'pria' => 0,
			                        'wanita' => 0
			                    ]
		                	],
		                	'secondary' => ['total' => [
			                        'pria' => 0,
			                        'wanita' => 0
			                    ]
		                	]
		                ];

		                if ($kelamin == 1) $dataResponse[$val->code_icd10][$primary]['total']['pria']++;
                		if ($kelamin == 2) $dataResponse[$val->code_icd10][$primary]['total']['wanita']++;

					}else {
						if ($kelamin == 1) $dataResponse[$val->code_icd10][$primary]['total']['pria']++;
                		if ($kelamin == 2) $dataResponse[$val->code_icd10][$primary]['total']['wanita']++;
					}
				}

				foreach ($dataResponse as $key2 => $val2) {
		            if (!isset($val2['primary']['total']['pria'])) $val2['primary']['total']['pria'] = 0;
		            if (!isset($val2['primary']['total']['wanita'])) $val2['primary']['total']['wanita'] = 0;
		            if (!isset($val2['secondary']['total']['pria'])) $val2['secondary']['total']['pria'] = 0;
		            if (!isset($val2['secondary']['total']['wanita'])) $val2['secondary']['total']['wanita'] = 0;

		            $dataResponse[$key2]['total'] = $val2['primary']['total']['pria'] + $val2['primary']['total']['wanita'] + $val2['secondary']['total']['pria'] + $val2['secondary']['total']['wanita'];

		            $sort[$key2] = $dataResponse[$key2]['total'];
		        }

		        arsort($sort);
		        foreach ($sort as $key3 => $val3) {
		            $new_response[$key3] = $dataResponse[$key3];
		        }

		        $res['data'] = $new_response;
			}
			$this->response($res, $res['status']);
		}

		$res['status']  = 500;
		$errors = $this->form_validation->error_array();
		$res['message'] = $errors;
		$this->response($res, $res['status']);
	}
}
