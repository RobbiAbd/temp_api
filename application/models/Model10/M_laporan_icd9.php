<?php

class M_laporan_icd9 extends CI_Model
{

  private function get_datatables_query($column_order,$column_search,$order,$search, $sortby_column, $sortby_type,$offset,$limit)
  {
    $i = 0;

    foreach ($column_search as $item)
    {
      if($search !== '-')
      {
        if($i == 0)
        {
          $this->db->group_start();
          $this->db->like($item,$search);
        }
        else
        {
          $this->db->or_like($item,$search);
        }

        if(count($column_search) - 1 == $i)
        $this->db->group_end();
      }
      $i++;
    }

    if($sortby_column)
    {
      $this->db->order_by($column_order[$sortby_column], $sortby_type);
    }

    else if($order)
    {
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_filtered_total($periode_start, $periode_end, $jenis_periode, $column_order,$column_search,$order,$search, $sortby_column, $sortby_type, $offset, $limit)
  {

    $this->db->select('notes_icd9.code_icd9 code_icd9,
      ref_icd9.nama_icd9 nama_icd9,
      visit.checkin_time checkin_time,
      visit.checkout_time checkout_time,
      user_profile.gender gender
    ')

    ->from('notes_icd9 notes_icd9')
    ->join('ref_icd9 ref_icd9', 'notes_icd9.code_icd9 = ref_icd9.code_icd9')
    ->join('pasien_visit visit', 'visit.id_pasien_visit = notes_icd9.id_visit')
    ->join('pasien_registrasi registrasi', 'visit.id_pasien_registrasi = registrasi.id_pasien_registrasi')
    ->join('users_profile user_profile', 'registrasi.id_users_pasien = user_profile.user_id', 'left')
    ->where([
      'visit.del_date' => NULL
    ]);

    if (isset($jenis_periode) && $jenis_periode == '1' && ($periode_end. ' 23:59:59' > $periode_start)) {
            $this->db->where([
              'date(visit.checkin_time) >=' => $periode_start,
              'date(visit.checkin_time) <=' => $periode_end . ' 23:59:59'
            ]);

    } else if (isset($jenis_periode) && $jenis_periode == '2' && ($periode_end. ' 23:59:59' > $periode_start)) {
            $this->db->where([
              'date(visit.checkout_time) >=' => $periode_start,
              'date(visit.checkout_time) <=' => $periode_end . ' 23:59:59'
            ]);
    }


    $this->get_datatables_query($column_order,$column_search,$order,$search, $sortby_column, $sortby_type,$offset,$limit);

    return $this->db->count_all_results();
  }

  function get_total($periode_start, $periode_end, $jenis_periode)
  {

    $this->db->select('notes_icd9.code_icd9 code_icd9,
      ref_icd9.nama_icd9 nama_icd9,
      visit.checkin_time checkin_time,
      visit.checkout_time checkout_time,
      user_profile.gender gender
    ')

    ->from('notes_icd9 notes_icd9')
    ->join('ref_icd9 ref_icd9', 'notes_icd9.code_icd9 = ref_icd9.code_icd9')
    ->join('pasien_visit visit', 'visit.id_pasien_visit = notes_icd9.id_visit')
    ->join('pasien_registrasi registrasi', 'visit.id_pasien_registrasi = registrasi.id_pasien_registrasi')
    ->join('users_profile user_profile', 'registrasi.id_users_pasien = user_profile.user_id', 'left')
    ->where([
      'visit.del_date' => NULL
    ]);

    if (isset($jenis_periode) && $jenis_periode == '1' && ($periode_end. ' 23:59:59' > $periode_start)) {
            $this->db->where([
              'date(visit.checkin_time) >=' => $periode_start,
              'date(visit.checkin_time) <=' => $periode_end . ' 23:59:59'
            ]);

    } else if (isset($jenis_periode) && $jenis_periode == '2' && ($periode_end. ' 23:59:59' > $periode_start)) {
            $this->db->where([
              'date(visit.checkout_time) >=' => $periode_start,
              'date(visit.checkout_time) <=' => $periode_end . ' 23:59:59'
            ]);
    }


    return $this->db->count_all_results();
  }

  public function get($params)
  {

    $column_order  = array("no", "notes_icd9.code_icd9","ref_icd9.nama_icd9","user_profile.gender");

    $column_search = array("notes_icd9.code_icd9","ref_icd9.nama_icd9");

    $order = array("notes_icd9.code_icd9" => "DESC");

    $this->db->select('notes_icd9.code_icd9 code_icd9,
      ref_icd9.nama_icd9 nama_icd9,
      visit.checkin_time checkin_time,
      visit.checkout_time checkout_time,
      user_profile.gender gender
    ')

    ->from('notes_icd9 notes_icd9')
    ->join('ref_icd9 ref_icd9', 'notes_icd9.code_icd9 = ref_icd9.code_icd9')
    ->join('pasien_visit visit', 'visit.id_pasien_visit = notes_icd9.id_visit')
    ->join('pasien_registrasi registrasi', 'visit.id_pasien_registrasi = registrasi.id_pasien_registrasi')
    ->join('users_profile user_profile', 'registrasi.id_users_pasien = user_profile.user_id', 'left')
    ->where([
      'visit.del_date' => NULL
    ]);


    $this->get_datatables_query($column_order, $column_search, $order, $params['search'], $params['sortby_column'], $params['sortby_type'],$params['offset'], $params['limit']);

    if($params['limit'] > 0)
    $this->db->limit($params['limit'], $params['offset']);

    $sql    = $this->db->get();
    $result = $sql->result();

    return [
      'result'        => $result,
      'record_total'  => $this->get_total($params['periode_start'], $params['periode_end'], $params['jenis_periode']),
      'record_filter' => $this->get_filtered_total($params['periode_start'], $params['periode_end'], $params['jenis_periode'], $column_order, $column_search, $order, $params['search'], $params['sortby_column'], $params['sortby_type'],$params['offset'], $params['limit'])
    ];
  }


}
