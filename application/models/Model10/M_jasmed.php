<?php

class M_jasmed extends CI_Model
{

  private function get_datatables_query($column_order,$column_search,$order,$search, $sortby_column, $sortby_type,$offset,$limit)
  {
    $i = 0;

    foreach ($column_search as $item)
    {
      if($search !== '-')
      {
        if($i == 0)
        {
          $this->db->group_start();
          $this->db->like($item,$search);
        }
        else
        {
          $this->db->or_like($item,$search);
        }

        if(count($column_search) - 1 == $i)
        $this->db->group_end();
      }
      $i++;
    }

    if($sortby_column)
    {
      $this->db->order_by($column_order[$sortby_column], $sortby_type);
    }

    else if($order)
    {
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_filtered_total($periode_start, $periode_end, $column_order,$column_search,$order,$search, $sortby_column, $sortby_type, $offset, $limit)
  {

    $this->db->select('visit.id_pasien_visit id_visit,
    visit.no_visit no_visit,
    registrasi.code_icd10_awal icd_10_awal,
    ref_icd10.nama_icd10  nama_icd10_awal,
    visit.checkin_time tanggal_checkin,
    visit.checkin_petugas nama_petugas_checkin,
    visit.checkout_time tanggal_checkout,
    visit.checkout_petugas nama_petugas_checkout,
    registrasi.is_pasien_baru,
    
    user_profile.dob tanggal_lahir,
    user_profile.gender kelamin,
    user_profile.full_name nama_pasien,
    user_profile.bpjs_id no_bpjs,
    user_profile.address alamat1,
    user_profile.no_rm no_rm,
    
    kelurahan.nama_kelurahan,
    kecamatan.nama_kecamatan, 
    kabupaten.nama_kabupaten,
    provinsi.nama_provinsi,
    
    payment.payment,
    checkout.level,
    checkout.nama alasan_pulang,
    
    dokter_profile.full_name nama_pj_ruangan,
    visit.id_hrd id_pj_ruangan,
    
    departements.departement_name nama_poli,

    visit.dirujuk_id_poli as id_dept_asal,
    dept_asal.departement_name as nama_dept_asal,
    visit.id_dept_tujuan as id_dept_tujuan,
    dept_tujuan.departement_name as nama_dept_tujuan,
    
    dept.total_tarif as total_tarif_dept,
    rad.total_tarif as total_tarif_rad,
    lab.total_tarif as total_tarif_lab
    ')

    ->from('pasien_visit visit')
    ->join('pasien_registrasi registrasi','registrasi.id_pasien_registrasi = visit.id_pasien_registrasi','left')
    ->join('users_profile user_profile','registrasi.id_users_pasien = user_profile.user_id','left')
    ->join('ref_address_kelurahan kelurahan','user_profile.id_kel = kelurahan.id','left')
    ->join('ref_address_kecamatan kecamatan','kelurahan.id_ref_address_kecamatan = kecamatan.id','left')
    ->join('ref_address_kabupaten kabupaten','kecamatan.id_ref_address_kabupaten = kabupaten.id','left')
    ->join('ref_address_provinsi provinsi','kabupaten.id_ref_address_provinsi = provinsi.id','left')
    ->join('ref_payment payment','registrasi.id_ref_payment = payment.id_ref_payment','left')
    ->join('ref_checkout checkout','registrasi.id_ref_checkout = checkout.id','left')
    ->join('users_profile dokter_profile','visit.id_hrd = dokter_profile.user_id','left')
    ->join('departements departements','visit.id_departemen = departements.departement_id')
    ->join('departements_group departements_group','departements_group.id = departements.type')

    ->join('ref_icd10 ref_icd10','registrasi.code_icd10_awal = ref_icd10.code_icd10','left')

    ->join('departements dept_asal','visit.dirujuk_id_poli = dept_asal.departement_id','left')
    ->join('departements_group dept_group_asal','dept_group_asal.id = dept_asal.type','left')

    ->join('departements dept_tujuan','visit.id_dept_tujuan = dept_tujuan.departement_id','left')
    ->join('departements_group dept_group_tujuan','dept_group_tujuan.id = dept_tujuan.type','left')

    ->join('(
      select 
        visit.id_pasien_visit,
        SUM(payment_dept_detail.nilai_payment) as total_tarif
        FROM pasien_visit as visit
        LEFT JOIN order_dept as order_dept on order_dept.id_visit = visit.id_pasien_visit
        LEFT JOIN order_dept_details as order_dept_details on order_dept_details.id_order_dept = order_dept.id
        LEFT JOIN payment_dept_detail as payment_dept_detail on payment_dept_detail.id_order_dept_details = order_dept_details.id
        GROUP BY visit.id_pasien_visit
    ) dept','dept.id_pasien_visit = visit.id_pasien_visit','left')

    ->join('(
    select 
      visit.id_pasien_visit,
      SUM(payment_rad_detail.nilai_payment) as total_tarif
      FROM pasien_visit as visit
      LEFT JOIN order_rad as order_rad on order_rad.id_visit = visit.id_pasien_visit
      LEFT JOIN order_rad_details as order_rad_details on order_rad_details.id_order_rad = order_rad.id
      LEFT JOIN payment_rad_detail as payment_rad_detail on payment_rad_detail.id_order_rad_details = order_rad_details.id
      GROUP BY visit.id_pasien_visit
    ) rad','rad.id_pasien_visit = visit.id_pasien_visit','left')

    ->join('(
    select 
      visit.id_pasien_visit,
      SUM(payment_lab_detail.nilai_payment) as total_tarif
      FROM pasien_visit as visit
      LEFT JOIN order_lab as order_lab on order_lab.id_visit = visit.id_pasien_visit
      LEFT JOIN order_lab_details as order_lab_details on order_lab_details.id_order_lab = order_lab.id
      LEFT JOIN payment_lab_detail as payment_lab_detail on payment_lab_detail.id_order_lab_details = order_lab_details.id
      GROUP BY visit.id_pasien_visit
    ) lab','lab.id_pasien_visit = visit.id_pasien_visit','left')

    ->where([
      'date(visit.checkin_time) >=' => $periode_start,
      'date(visit.checkout_time) <=' => $periode_end
    ]);


    $this->get_datatables_query($column_order,$column_search,$order,$search, $sortby_column, $sortby_type,$offset,$limit);

    return $this->db->count_all_results();
  }

  function get_total($periode_start, $periode_end)
  {

    $this->db->select('visit.id_pasien_visit id_visit,
    visit.no_visit no_visit,
    registrasi.code_icd10_awal icd_10_awal,
    ref_icd10.nama_icd10  nama_icd10_awal,
    visit.checkin_time tanggal_checkin,
    visit.checkin_petugas nama_petugas_checkin,
    visit.checkout_time tanggal_checkout,
    visit.checkout_petugas nama_petugas_checkout,
    registrasi.is_pasien_baru,
    
    user_profile.dob tanggal_lahir,
    user_profile.gender kelamin,
    user_profile.full_name nama_pasien,
    user_profile.bpjs_id no_bpjs,
    user_profile.address alamat1,
    user_profile.no_rm no_rm,
    
    kelurahan.nama_kelurahan,
    kecamatan.nama_kecamatan, 
    kabupaten.nama_kabupaten,
    provinsi.nama_provinsi,
    
    payment.payment,
    checkout.level,
    checkout.nama alasan_pulang,
    
    dokter_profile.full_name nama_pj_ruangan,
    visit.id_hrd id_pj_ruangan,
    
    departements.departement_name nama_poli,

    visit.dirujuk_id_poli as id_dept_asal,
    dept_asal.departement_name as nama_dept_asal,
    visit.id_dept_tujuan as id_dept_tujuan,
    dept_tujuan.departement_name as nama_dept_tujuan,
    
    dept.total_tarif as total_tarif_dept,
    rad.total_tarif as total_tarif_rad,
    lab.total_tarif as total_tarif_lab
    ')

    ->from('pasien_visit visit')
    ->join('pasien_registrasi registrasi','registrasi.id_pasien_registrasi = visit.id_pasien_registrasi','left')
    ->join('users_profile user_profile','registrasi.id_users_pasien = user_profile.user_id','left')
    ->join('ref_address_kelurahan kelurahan','user_profile.id_kel = kelurahan.id','left')
    ->join('ref_address_kecamatan kecamatan','kelurahan.id_ref_address_kecamatan = kecamatan.id','left')
    ->join('ref_address_kabupaten kabupaten','kecamatan.id_ref_address_kabupaten = kabupaten.id','left')
    ->join('ref_address_provinsi provinsi','kabupaten.id_ref_address_provinsi = provinsi.id','left')
    ->join('ref_payment payment','registrasi.id_ref_payment = payment.id_ref_payment','left')
    ->join('ref_checkout checkout','registrasi.id_ref_checkout = checkout.id','left')
    ->join('users_profile dokter_profile','visit.id_hrd = dokter_profile.user_id','left')
    ->join('departements departements','visit.id_departemen = departements.departement_id')
    ->join('departements_group departements_group','departements_group.id = departements.type')

    ->join('ref_icd10 ref_icd10','registrasi.code_icd10_awal = ref_icd10.code_icd10','left')

    ->join('departements dept_asal','visit.dirujuk_id_poli = dept_asal.departement_id','left')
    ->join('departements_group dept_group_asal','dept_group_asal.id = dept_asal.type','left')

    ->join('departements dept_tujuan','visit.id_dept_tujuan = dept_tujuan.departement_id','left')
    ->join('departements_group dept_group_tujuan','dept_group_tujuan.id = dept_tujuan.type','left')

    ->join('(
      select 
        visit.id_pasien_visit,
        SUM(payment_dept_detail.nilai_payment) as total_tarif
        FROM pasien_visit as visit
        LEFT JOIN order_dept as order_dept on order_dept.id_visit = visit.id_pasien_visit
        LEFT JOIN order_dept_details as order_dept_details on order_dept_details.id_order_dept = order_dept.id
        LEFT JOIN payment_dept_detail as payment_dept_detail on payment_dept_detail.id_order_dept_details = order_dept_details.id
        GROUP BY visit.id_pasien_visit
    ) dept','dept.id_pasien_visit = visit.id_pasien_visit','left')

    ->join('(
    select 
      visit.id_pasien_visit,
      SUM(payment_rad_detail.nilai_payment) as total_tarif
      FROM pasien_visit as visit
      LEFT JOIN order_rad as order_rad on order_rad.id_visit = visit.id_pasien_visit
      LEFT JOIN order_rad_details as order_rad_details on order_rad_details.id_order_rad = order_rad.id
      LEFT JOIN payment_rad_detail as payment_rad_detail on payment_rad_detail.id_order_rad_details = order_rad_details.id
      GROUP BY visit.id_pasien_visit
    ) rad','rad.id_pasien_visit = visit.id_pasien_visit','left')

    ->join('(
    select 
      visit.id_pasien_visit,
      SUM(payment_lab_detail.nilai_payment) as total_tarif
      FROM pasien_visit as visit
      LEFT JOIN order_lab as order_lab on order_lab.id_visit = visit.id_pasien_visit
      LEFT JOIN order_lab_details as order_lab_details on order_lab_details.id_order_lab = order_lab.id
      LEFT JOIN payment_lab_detail as payment_lab_detail on payment_lab_detail.id_order_lab_details = order_lab_details.id
      GROUP BY visit.id_pasien_visit
    ) lab','lab.id_pasien_visit = visit.id_pasien_visit','left')

    ->where([
      'date(visit.checkin_time) >=' => $periode_start,
      'date(visit.checkout_time) <=' => $periode_end
    ]);


    return $this->db->count_all_results();
  }

  public function get($params)
  {

    $column_order  = array("no", "visit.id_pasien_visit","registrasi.code_icd10_awal","ref_icd10.nama_icd10");

    $column_search = array("visit.id_pasien_visit","registrasi.code_icd10_awal","ref_icd10.nama_icd10");

    $order = array("visit.id_pasien_visit" => "DESC");

    $this->db->select('visit.id_pasien_visit id_visit,
    visit.no_visit no_visit,
    registrasi.code_icd10_awal icd_10_awal,
    ref_icd10.nama_icd10  nama_icd10_awal,
    visit.checkin_time tanggal_checkin,
    visit.checkin_petugas nama_petugas_checkin,
    visit.checkout_time tanggal_checkout,
    visit.checkout_petugas nama_petugas_checkout,
    registrasi.is_pasien_baru,
    
    user_profile.dob tanggal_lahir,
    user_profile.gender kelamin,
    user_profile.full_name nama_pasien,
    user_profile.bpjs_id no_bpjs,
    user_profile.address alamat1,
    user_profile.no_rm no_rm,
    
    kelurahan.nama_kelurahan,
    kecamatan.nama_kecamatan, 
    kabupaten.nama_kabupaten,
    provinsi.nama_provinsi,
    
    payment.payment,
    checkout.level,
    checkout.nama alasan_pulang,
    
    dokter_profile.full_name nama_pj_ruangan,
    visit.id_hrd id_pj_ruangan,
    
    departements.departement_name nama_poli,

    visit.dirujuk_id_poli as id_dept_asal,
    dept_asal.departement_name as nama_dept_asal,
    visit.id_dept_tujuan as id_dept_tujuan,
    dept_tujuan.departement_name as nama_dept_tujuan,
    
    dept.total_tarif as total_tarif_dept,
    rad.total_tarif as total_tarif_rad,
    lab.total_tarif as total_tarif_lab
    ')

    ->from('pasien_visit visit')
    ->join('pasien_registrasi registrasi','registrasi.id_pasien_registrasi = visit.id_pasien_registrasi','left')
    ->join('users_profile user_profile','registrasi.id_users_pasien = user_profile.user_id','left')
    ->join('ref_address_kelurahan kelurahan','user_profile.id_kel = kelurahan.id','left')
    ->join('ref_address_kecamatan kecamatan','kelurahan.id_ref_address_kecamatan = kecamatan.id','left')
    ->join('ref_address_kabupaten kabupaten','kecamatan.id_ref_address_kabupaten = kabupaten.id','left')
    ->join('ref_address_provinsi provinsi','kabupaten.id_ref_address_provinsi = provinsi.id','left')
    ->join('ref_payment payment','registrasi.id_ref_payment = payment.id_ref_payment','left')
    ->join('ref_checkout checkout','registrasi.id_ref_checkout = checkout.id','left')
    ->join('users_profile dokter_profile','visit.id_hrd = dokter_profile.user_id','left')
    ->join('departements departements','visit.id_departemen = departements.departement_id')
    ->join('departements_group departements_group','departements_group.id = departements.type')

    ->join('ref_icd10 ref_icd10','registrasi.code_icd10_awal = ref_icd10.code_icd10','left')

    ->join('departements dept_asal','visit.dirujuk_id_poli = dept_asal.departement_id','left')
    ->join('departements_group dept_group_asal','dept_group_asal.id = dept_asal.type','left')

    ->join('departements dept_tujuan','visit.id_dept_tujuan = dept_tujuan.departement_id','left')
    ->join('departements_group dept_group_tujuan','dept_group_tujuan.id = dept_tujuan.type','left')

    ->join('(
      select 
        visit.id_pasien_visit,
        SUM(payment_dept_detail.nilai_payment) as total_tarif
        FROM pasien_visit as visit
        LEFT JOIN order_dept as order_dept on order_dept.id_visit = visit.id_pasien_visit
        LEFT JOIN order_dept_details as order_dept_details on order_dept_details.id_order_dept = order_dept.id
        LEFT JOIN payment_dept_detail as payment_dept_detail on payment_dept_detail.id_order_dept_details = order_dept_details.id
        GROUP BY visit.id_pasien_visit
    ) dept','dept.id_pasien_visit = visit.id_pasien_visit','left')

    ->join('(
    select 
      visit.id_pasien_visit,
      SUM(payment_rad_detail.nilai_payment) as total_tarif
      FROM pasien_visit as visit
      LEFT JOIN order_rad as order_rad on order_rad.id_visit = visit.id_pasien_visit
      LEFT JOIN order_rad_details as order_rad_details on order_rad_details.id_order_rad = order_rad.id
      LEFT JOIN payment_rad_detail as payment_rad_detail on payment_rad_detail.id_order_rad_details = order_rad_details.id
      GROUP BY visit.id_pasien_visit
    ) rad','rad.id_pasien_visit = visit.id_pasien_visit','left')

    ->join('(
    select 
      visit.id_pasien_visit,
      SUM(payment_lab_detail.nilai_payment) as total_tarif
      FROM pasien_visit as visit
      LEFT JOIN order_lab as order_lab on order_lab.id_visit = visit.id_pasien_visit
      LEFT JOIN order_lab_details as order_lab_details on order_lab_details.id_order_lab = order_lab.id
      LEFT JOIN payment_lab_detail as payment_lab_detail on payment_lab_detail.id_order_lab_details = order_lab_details.id
      GROUP BY visit.id_pasien_visit
    ) lab','lab.id_pasien_visit = visit.id_pasien_visit','left');


    $this->get_datatables_query($column_order, $column_search, $order, $params['search'], $params['sortby_column'], $params['sortby_type'],$params['offset'], $params['limit']);

    if($params['limit'] > 0)
    $this->db->limit($params['limit'], $params['offset']);

    $sql    = $this->db->get();
    $result = $sql->result();

    return [
      'result'        => $result,
      'record_total'  => $this->get_total($params['periode_start'], $params['periode_end']),
      'record_filter' => $this->get_filtered_total($params['periode_start'], $params['periode_end'], $column_order, $column_search, $order, $params['search'], $params['sortby_column'], $params['sortby_type'],$params['offset'], $params['limit'])
    ];
  }


}
