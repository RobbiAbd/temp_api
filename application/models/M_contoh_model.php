<?php

class M_contoh_model extends CI_Model
{

  private function get_datatables_query($column_order,$column_search,$order,$search, $sortby_column, $sortby_type,$offset,$limit)
  {
    $i = 0;

    foreach ($column_search as $item)
    {
      if($search !== '-')
      {
        if($i == 0)
        {
          $this->db->group_start();
          $this->db->like($item,$search);
        }
        else
        {
          $this->db->or_like($item,$search);
        }

        if(count($column_search) - 1 == $i)
        $this->db->group_end();
      }
      $i++;
    }

    if($sortby_column)
    {
      $this->db->order_by($column_order[$sortby_column], $sortby_type);
    }

    else if($order)
    {
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_filtered_total($periode_start, $periode_end, $column_order,$column_search,$order,$search, $sortby_column, $sortby_type, $offset, $limit)
  {

    $this->db->select("a.*")
    ->from('notes_icd10 a')
    ->join('ref_icd10 b', 'b.code_icd10 = a.code_icd10', 'left')
    ->join('pasien_visit c', 'c.id_pasien_visit = a.id_visit', 'left')
    ->join('pasien_registrasi d', 'd.id_pasien_registrasi = c.id_pasien_registrasi', 'left')
    ->join('users_profile e', 'e.user_id = d.id_users_pasien', 'left')
    ->join('departements f', 'f.departement_id = c.id_departemen', 'left')
    ->join('departements_group g', 'g.id = f.type')
    ->where([
      'c.del_date' => null,
      'date(c.checkin_time) >=' => $periode_start,
      'date(c.checkin_time) <=' => $periode_end,
      'g.group' => '0'
    ]);

    $this->db->group_by('a.code_icd10');

    $this->get_datatables_query($column_order,$column_search,$order,$search, $sortby_column, $sortby_type,$offset,$limit);

    return $this->db->count_all_results();
  }

  function get_total($periode_start, $periode_end)
  {

    // $poli = rawurldecode($poli);
    $this->db->select("a.*")
    ->from('notes_icd10 a')
    ->join('ref_icd10 b', 'b.code_icd10 = a.code_icd10', 'left')
    ->join('pasien_visit c', 'c.id_pasien_visit = a.id_visit', 'left')
    ->join('pasien_registrasi d', 'd.id_pasien_registrasi = c.id_pasien_registrasi', 'left')
    ->join('users_profile e', 'e.user_id = d.id_users_pasien', 'left')
    ->join('departements f', 'f.departement_id = c.id_departemen', 'left')
    ->join('departements_group g', 'g.id = f.type')
    ->where([
      'c.del_date' => null,
      'date(c.checkin_time) >=' => $periode_start,
      'date(c.checkin_time) <=' => $periode_end,
      'g.group' => '0'
    ]);

    $this->db->group_by('a.code_icd10');

    return $this->db->count_all_results();
  }

  public function get($params)
  {

    $column_order  = array("no", "a.code_icd10","b.nama_icd10","jumlah_pria", "jumlah_wanita", "total_pasien");

    $column_search = array("a.code_icd10","b.nama_icd10");

    $order = array("total_pasien" => "DESC");

    $this->db->select('a.code_icd10 icd_code, b.nama_icd10 deskripsi,
    SUM(IF(e.gender = "1", 1, 0)) jumlah_pria,
    SUM(IF(e.gender = "2", 1, 0)) jumlah_wanita,
    COUNT(a.id) total_pasien
    ')

    ->from('notes_icd10 a')
    ->join('ref_icd10 b', 'b.code_icd10 = a.code_icd10', 'left')
    ->join('pasien_visit c', 'c.id_pasien_visit = a.id_visit', 'left')
    ->join('pasien_registrasi d', 'd.id_pasien_registrasi = c.id_pasien_registrasi', 'left')
    ->join('users_profile e', 'e.user_id = d.id_users_pasien', 'left')
    ->join('departements f', 'f.departement_id = c.id_departemen', 'left')
    ->join('departements_group g', 'g.id = f.type')
    ->where([
      'c.del_date' => null,
      'date(c.checkin_time) >=' => $params['periode_start'],
      'date(c.checkin_time) <=' => $params['periode_end'],
      'g.group' => '0'
    ]);
    $this->db->group_by('a.code_icd10');

    $this->get_datatables_query($column_order, $column_search, $order, $params['search'], $params['sortby_column'], $params['sortby_type'],$params['offset'], $params['limit']);

    if($params['limit'] > 0)
    $this->db->limit($params['limit'], $params['offset']);

    $sql    = $this->db->get();
    $result = $sql->result();

    return [
      'result'        => $result,
      'record_total'  => $this->get_total($params['periode_start'], $params['periode_end']),
      'record_filter' => $this->get_filtered_total($params['periode_start'], $params['periode_end'], $column_order, $column_search, $order, $params['search'], $params['sortby_column'], $params['sortby_type'],$params['offset'], $params['limit'])
    ];
  }


}
